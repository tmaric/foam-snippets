/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2022 Tomislav Maric, TU Darmstadt
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    foamIsoSurfaceCurvature 

Description
    Compute the curvature using an iso-surface that generates triangulated 
    surface patches that are local to each cell. 

\*---------------------------------------------------------------------------*/

#include "IOobject.H"
#include "error.H"
#include "fvCFD.H"

#include "isoSurfaceCell.H"
#include "isoSurfaceTopo.H"
#include "isoSurfacePoint.H"
#include "volPointInterpolation.H"
#include <cstdlib>
#include <iomanip>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

bool is_zero(vector const& vec)
{
    return ((vec[0] == 0) && (vec[1] == 0) && (vec[2] == 0));
}

bool is_non_zero(vector const& vec)
{
    return ! is_zero(vec); 
}

int main(int argc, char *argv[])
{
    argList::addOption
    (
        "field",
        "string",
        "Name of the volume (cell-centered) field whose curvature is approximated."
    );

    #include "setRootCase.H"
    #include "createTime.H"
    #include "createMesh.H"

    const word fieldName = args.get<word>("field");

    #include "createFields.H"

    volPointInterpolation vpInterp(mesh); 
    
    // TODO: read from constant / transport 
    //IOdictionary transportPropertie...
    const double sigma = 72.75e-03;

    tmp<pointScalarField> psiPointTmp = vpInterp.interpolate(psi);
    pointScalarField& psiPoint = psiPointTmp.ref();
    psiPoint.rename(fieldName + ".point");
    psiPoint.write();

    isoSurfaceTopo isoTopo(
        mesh, 
        psi, 
        psiPoint, 
        0.5
    );
    std::stringstream ss; 
    ss << "isoTopo-" << std::setw(8) << std::setfill('0') 
        << Pstream::myProcNo() << ".vtk";
    isoTopo.write(ss.str());

    const auto& faceAreas = isoTopo.faceAreas();
    const auto& pointNormals = isoTopo.pointNormals();
    const auto& points = isoTopo.points(); 
    const auto& meshCells = isoTopo.meshCells();
    const auto& magFaceAreas = isoTopo.magFaceAreas();
    const auto& V = mesh.V();
    forAll(isoTopo, faceI)
    {
        const auto& isoFace = isoTopo[faceI]; 
        vector kappaSum (0,0,0);
        for (decltype(isoFace.size()) pI0 = 0; pI0 < isoFace.size(); ++pI0)
        {
            auto pI1 = (pI0 + 1) % isoFace.size(); 
            const auto& pG0 = isoFace[pI0]; // Global p0 label
            const auto& pG1 = isoFace[pI1]; // Global p1 label

            const auto& p0 = points[pG0];
            const auto& p1 = points[pG1];

            vector edgeNormal = 0.5 * (pointNormals[pG0] + pointNormals[pG1]); 
            edgeNormal /= mag(edgeNormal);
            vector edgeVector = p1 - p0;
            kappaSum += (edgeVector ^ edgeNormal); 
        }
        
        // TODO: 
        // - surface tension sigma as volScalar and surfaceScalar field
        // - check division with |\Omega_c|
        fSigma[meshCells[faceI]] = sigma * kappaSum / V[meshCells[faceI]];
        
        // The isoSurface normals are oriented in the direction of the 
        // level set field gradient, so there is no need to multiply with -1 TM.
        kappa[meshCells[faceI]] = (kappaSum & faceAreas[faceI]) /  
            Foam::sqr(magFaceAreas[faceI]);
    }
    
    // Average surface tension - destroys conservativeness
    volVectorField fSigmaSmooth ("fSigmaSmooth", fSigma);
    fSigmaSmooth = fvc::average(fSigmaSmooth);
    fSigmaSmooth = fvc::average(fSigmaSmooth);
    fSigmaSmooth = fvc::average(fSigmaSmooth);
    //for (label  i = 0; i < 1; ++i)
    //{
        //fSigmaSmooth = fvc::average(fSigmaSmooth);
        //forAll(fSigma, cellI)
        //{
            //if(is_non_zero(fSigma[cellI]))
                //fSigmaSmooth[cellI] = fSigma[cellI];
        //}
    //}
    fSigma = fSigmaSmooth;
    
    // Compute face-centered surface tension
    const auto& own = mesh.owner(); 
    const auto& nei = mesh.neighbour(); 
    
    forAll (own, faceI)
    {
        if (is_non_zero(fSigma[own[faceI]]) && 
            is_non_zero(fSigma[nei[faceI]])) 
        {
            fSigmaFace[faceI] = 0.5*(fSigma[own[faceI]] + fSigma[nei[faceI]]);
        }
    } 
    
    // Set face-centered surface tension across MPI process boundaries. 
    auto& fSigmaBdryField = fSigma.boundaryFieldRef(); // needed for Nc LLSQ contribs
    const auto& patches = mesh.boundary(); // needed for Nc LLSQ contribs
    const auto& faceOwner = mesh.faceOwner();
    forAll(fSigmaBdryField, patchI)
    {
        const fvPatch& patch = patches[patchI];
        if (isA<processorFvPatch>(patch)) // MPI patch 
        {
            auto& fSigmaPatchField = fSigmaBdryField[patchI]; 
            auto fSigmaPatchNeiFieldTmp = fSigmaPatchField.patchNeighbourField();
            const auto& fSigmaPatchNeiField = fSigmaPatchNeiFieldTmp();
            forAll(fSigmaPatchNeiField, faceI)
            {
                label faceJ = faceI + patch.start(); // Global face label.
                if (is_non_zero(fSigma[faceOwner[faceJ]]) &&
                    is_non_zero(fSigmaPatchNeiField[faceI]))
                {
                    fSigmaPatchField[faceI] = 0.5*(fSigma[faceOwner[faceJ]] + 
                        fSigmaPatchNeiField[faceI]);
                }
            }
        }
    }
    
    kappa.write();
    fSigma.write();
    fSigmaFace.write();
    
    Info << "gSum(fSigma) = " << gSum(fSigma) << endl;
    Info << "max(mag(fSigma)) = " << max(mag(fSigma)).value() << endl;
    Info << "gSum(fSigmaFace) = " << gSum(fSigmaFace) << endl;
    Info << "max(mag(fSigmaFace)) = " << max(mag(fSigmaFace)).value() << endl;
    
    // Solving the Young-Laplace Equation
    fvScalarMatrix YoungLaplaceEqn
    (
        fvm::laplacian(p_rgh) == fvc::div(fSigmaFace & mesh.Sf())
    );
    YoungLaplaceEqn.solve();
    p_rgh.write();
    
    // Young-Laplace error 
    
    const IOdictionary setAlphaFieldDict
    (
        IOobject
        (
            "setAlphaFieldDict", 
            "system", 
            runTime,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );
    
    const scalar radius = setAlphaFieldDict.get<scalar>("radius");

    dimensionedScalar p_exact(
        "p_exact", 
        p_rgh.dimensions(), 
        2*sigma/radius 
    );
    
    scalar deltaX = Foam::max(Foam::pow(mesh.deltaCoeffs(), -1)).value(); 
    scalar eMaxP = Foam::max(Foam::mag(p_rgh - p_exact)).value(); 
    
    OFstream errorFile("YoungLaplace.csv");
    errorFile << "DELTA_X,MAX_PRESSURE_ERROR\n" 
        << deltaX << "," << eMaxP << "\n";
    
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< nl;
    runTime.printExecutionTime(Info);

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //


    //const auto& Sf = isoTopo.faceNormals(); 
    //const auto& meshCells = isoTopo.meshCells();
    //forAll(meshCells, polygonI)
    //{
        //nc[meshCells[polygonI]] = Sf[polygonI];
    //}
    
    //volScalarField ncMag (Foam::mag(nc));
    //forAll(nc, cellI)
    //{
        //if (ncMag[cellI] > SMALL)
            //nc[cellI] /= ncMag[cellI];
    //}
    //surfaceVectorField nf (fvc::interpolate(nc));